# Polish translation of cbrt.3 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 1995 Jim Van Zandt ("manpages-1")
# Copyright © of Polish translation:
# Adam Byrtek (PTM) <abyrtek@priv.onet.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2013, 2017.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2017-02-07 21:13+0100\n"
"PO-Revision-Date: 2017-02-07 21:36+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10"
" || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#, no-wrap
msgid "CBRT"
msgstr "CBRT"

#. type: TH
#, no-wrap
msgid "2016-03-15"
msgstr "2016-03-15"

#. type: TH
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "cbrt, cbrtf, cbrtl - cube root function"
msgstr "cbrt, cbrtf, cbrtl - pierwiastek sześcienny"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#, no-wrap
msgid "B<double cbrt(double >I<x>B<);>\n"
msgstr "B<double cbrt(double >I<x>B<);>\n"

#. type: Plain text
#, no-wrap
msgid "B<float cbrtf(float >I<x>B<);>\n"
msgstr "B<float cbrtf(float >I<x>B<);>\n"

#. type: Plain text
#, no-wrap
msgid "B<long double cbrtl(long double >I<x>B<);>\n"
msgstr "B<long double cbrtl(long double >I<x>B<);>\n"

#. type: Plain text
msgid "Link with I<-lm>."
msgstr "Proszę linkować z I<-lm>."

#. type: Plain text
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
msgid "B<cbrt>():"
msgstr "B<cbrt>():"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Od glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
msgid "B<cbrtf>(), B<cbrtl>():"
msgstr "B<cbrtf>(), B<cbrtl>():"

#. type: Plain text
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Od glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid ""
"These functions return the (real) cube root of I<x>.  This function cannot "
"fail; every representable real value has a representable real cube root."
msgstr ""
"Funkcje te zwracają (rzeczywisty) pierwiastek sześcienny liczby I<x>. Ta "
"funkcja nie może zawieść, każda reprezentowalna liczba rzeczywista ma swój "
"rzeczywisty pierwiastek sześcienny jako reprezentowalną liczbę rzeczywistą."

#. type: SH
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
msgid "These functions return the cube root of I<x>."
msgstr "Funkcje te zwracają pierwiastek sześcienny z I<x>."

#. type: Plain text
msgid ""
"If I<x> is +0, -0, positive infinity, negative infinity, or NaN, I<x> is "
"returned."
msgstr ""
"Jeśli I<x> jest równy +0, -0, dodatniej lub ujemnej nieskończoności lub NaN, "
"to zwracane jest I<x>."

#. type: SH
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: Plain text
msgid "No errors occur."
msgstr "Nie występują."

#. type: SH
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#, no-wrap
msgid ""
"B<cbrt>(),\n"
"B<cbrtf>(),\n"
"B<cbrtl>()\n"
msgstr ""
"B<cbrt>(),\n"
"B<cbrtf>(),\n"
"B<cbrtl>()\n"

#. type: tbl table
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#.  .BR cbrt ()
#.  was a GNU extension. It is now a C99 requirement.
#. type: Plain text
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid "B<pow>(3), B<sqrt>(3)"
msgstr "B<pow>(3), B<sqrt>(3)"

#. type: SH
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
msgid ""
"This page is part of release 4.07 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.07 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
