# Polish translation of atanh.3 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 1993 David Metcalfe, 2008 Linux Foundation ("manpages-1")
# Copyright © of Polish translation:
# Adam Byrtek (PTM) <abyrtek@priv.onet.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2004, 2013, 2017.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2017-02-07 21:13+0100\n"
"PO-Revision-Date: 2017-02-07 21:33+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10"
" || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#, no-wrap
msgid "ATANH"
msgstr "ATANH"

#. type: TH
#, no-wrap
msgid "2016-03-15"
msgstr "2016-03-15"

#. type: TH
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "atanh, atanhf, atanhl - inverse hyperbolic tangent function"
msgstr "atanh, atanhf, atanhl - funkcja odwrotna do tangensa hiperbolicznego"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#, no-wrap
msgid "B<double atanh(double >I<x>B<);>\n"
msgstr "B<double atanh(double >I<x>B<);>\n"

#. type: Plain text
#, no-wrap
msgid "B<float atanhf(float >I<x>B<);>\n"
msgstr "B<float atanhf(float >I<x>B<);>\n"

#. type: Plain text
#, no-wrap
msgid "B<long double atanhl(long double >I<x>B<);>\n"
msgstr "B<long double atanhl(long double >I<x>B<);>\n"

#. type: Plain text
msgid "Link with I<-lm>."
msgstr "Proszę linkować z I<-lm>."

#. type: Plain text
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
msgid "B<atanh>():"
msgstr "B<atanh>():"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Od glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
msgid "B<atanhf>(), B<atanhl>():"
msgstr "B<atanhf>(), B<atanhl>():"

#. type: Plain text
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Od glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc w wersji E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid ""
"These functions calculate the inverse hyperbolic tangent of I<x>; that is "
"the value whose hyperbolic tangent is I<x>."
msgstr ""
"Funkcje te obliczają funkcję odwrotną do tangensa hiperbolicznego dla I<x>, "
"to znaczy wartość, dla której tangens hiperboliczny wynosi I<x>."

#. type: SH
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
msgid ""
"On success, these functions return the inverse hyperbolic tangent of I<x>."
msgstr ""
"Funkcje te, gdy się zakończą pomyślnie, zwracają odwrotność tangensa "
"hiperbolicznego argumentu I<x>."

#. type: Plain text
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Jeśli I<x> wynosi NaN, to zwracane jest NaN."

#. type: Plain text
msgid "If I<x> is +0 (-0), +0 (-0) is returned."
msgstr "Jeśli I<x> wynosi +0 (-0), to zwracane jest +0 (-0)."

#. type: Plain text
msgid ""
"If I<x> is +1 or -1, a pole error occurs, and the functions return "
"B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, respectively, with the "
"mathematically correct sign."
msgstr ""
"Jeśli I<x> jest równe +1 lub -1, występuje błąd bieguna i funkcje "
"odpowiednio zwracają B<HUGE_VAL>, B<HUGE_VALF> lub B<HUGE_VALL> z poprawnie "
"ustawionym znakiem."

#.  POSIX.1-2001 documents an optional range error for subnormal x;
#.  glibc 2.8 does not do this.
#. type: Plain text
msgid ""
"If the absolute value of I<x> is greater than 1, a domain error occurs, and "
"a NaN is returned."
msgstr ""
"Jeśli wartość bezwzględna I<x> jest większa od 1, to występuje błąd "
"dziedziny i zwracane jest NaN."

#. type: SH
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: Plain text
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Informacje o tym, jak określić, czy wystąpił błąd podczas wywołania tych "
"funkcji, można znaleźć w podręczniku B<math_error>(7)."

#. type: Plain text
msgid "The following errors can occur:"
msgstr "Mogą wystąpić następujące błędy:"

#. type: TP
#, no-wrap
msgid "Domain error: I<x> less than -1 or greater than +1"
msgstr "Błąd dziedziny: I<x> jest mniejsze niż -1 lub większe niż +1"

#. type: Plain text
msgid ""
"I<errno> is set to B<EDOM>.  An invalid floating-point exception "
"(B<FE_INVALID>)  is raised."
msgstr ""
"I<errno> jest ustawiane na B<EDOM>. Rzucany jest wyjątek niepoprawnej "
"operacji zmiennoprzecinkowej (B<FE_INVALID>)."

#. type: TP
#, no-wrap
msgid "Pole error: I<x> is +1 or -1"
msgstr "Błąd bieguna: I<x> jest równe +1 lub -1"

#. type: Plain text
msgid ""
"I<errno> is set to B<ERANGE> (but see BUGS).  A divide-by-zero floating-"
"point exception (B<FE_DIVBYZERO>)  is raised."
msgstr ""
"I<errno> jest ustawiane na B<ERANGE> (patrz także BŁĘDY IMPLEMENTACJI). "
"Rzucany jest wyjątek zmiennoprzecinkowego dzielenia przez zero "
"(B<FE_DIVBYZERO>)."

#. type: SH
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#, no-wrap
msgid ""
"B<atanh>(),\n"
"B<atanhf>(),\n"
"B<atanhl>()\n"
msgstr ""
"B<atanh>(),\n"
"B<atanhf>(),\n"
"B<atanhl>()\n"

#. type: tbl table
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
msgid "The variant returning I<double> also conforms to SVr4, 4.3BSD, C89."
msgstr ""
"Wariant zwracający wartość typu I<double> jest zgodny również z SVr4, "
"4.3BSD, C89."

#. type: SH
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY IMPLEMENTACJI"

#.  Bug: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6759
#.  This can be seen in sysdeps/ieee754/k_standard.c
#. type: Plain text
msgid ""
"In glibc 2.9 and earlier, when a pole error occurs, I<errno> as set to "
"B<EDOM> instead of the POSIX-mandated B<ERANGE>.  Since version 2.10, glibc "
"does the right thing."
msgstr ""
"W wersji 2.9 i wcześniejszych biblioteki glibc w razie wystąpienia błędu "
"bieguna I<errno> jest ustawiane na B<EDOM> zamiast na B<ERANGE>, jak tego "
"wymaga standard POSIX. Zostało to poprawione w wersji 2.10 biblioteki glibc."

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid ""
"B<acosh>(3), B<asinh>(3), B<catanh>(3), B<cosh>(3), B<sinh>(3), B<tanh>(3)"
msgstr ""
"B<acosh>(3), B<asinh>(3), B<catanh>(3), B<cosh>(3), B<sinh>(3), B<tanh>(3)"

#. type: SH
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
msgid ""
"This page is part of release 4.07 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.07 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
