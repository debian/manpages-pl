# Polish translation of cacos.3 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 2002 Walter Harms, 2011 Michael Kerrisk (GPL-1)
# Copyright © of Polish translation:
# Robert Luberda (PTM) <robert@debian.org>, 2005, 2006, 2012, 2017.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2017-02-07 21:13+0100\n"
"PO-Revision-Date: 2017-02-07 21:36+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10"
" || n%100>=20) ? 1 : 2);\n"

#. type: TH
#, no-wrap
msgid "CACOS"
msgstr "CACOS"

#. type: TH
#, no-wrap
msgid "2015-04-19"
msgstr "2015-04-19"

#. type: TH
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "cacos, cacosf, cacosl - complex arc cosine"
msgstr "cacos, cacosf, cacosl - arcus cosinus liczb zespolonych"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
msgid "B<#include E<lt>complex.hE<gt>>"
msgstr "B<#include E<lt>complex.hE<gt>>"

#. type: Plain text
msgid "B<double complex cacos(double complex >I<z>B<);>"
msgstr "B<double complex cacos(double complex >I<z>B<);>"

#. type: Plain text
msgid "B<float complex cacosf(float complex >I<z>B<);>"
msgstr "B<float complex cacosf(float complex >I<z>B<);>"

#. type: Plain text
msgid "B<long double complex cacosl(long double complex >I<z>B<);>"
msgstr "B<long double complex cacosl(long double complex >I<z>B<);>"

#. type: Plain text
msgid "Link with I<-lm>."
msgstr "Proszę linkować z I<-lm>."

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid ""
"These functions calculate the complex arc cosine of I<z>.  If I<y\\ =\\ "
"cacos(z)>, then I<z\\ =\\ ccos(y)>.  The real part of I<y> is chosen in the "
"interval [0,pi]."
msgstr ""
"Funkcje te obliczają arcus cosinus liczby zespolonej I<z>. Jeżeli I<y\\ =\\ "
"cacos(z)>, to I<z\\ =\\ ccos(y)>. Część rzeczywista I<y> jest wybierana z "
"przedziału [0,pi]"

#. type: Plain text
msgid "One has:"
msgstr "Wzór:"

#. type: Plain text
#, no-wrap
msgid "    cacos(z) = -i * clog(z + i * csqrt(1 - z * z))\n"
msgstr "    cacos(z) = -i * clog(z + i * csqrt(1 - z * z))\n"

#. type: SH
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#. type: Plain text
msgid "These functions first appeared in glibc in version 2.1."
msgstr "Funkcje te pojawiły się po raz pierwszy w wersji 2.1 biblioteki glibc."

#. type: SH
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#, no-wrap
msgid ""
"B<cacos>(),\n"
"B<cacosf>(),\n"
"B<cacosl>()\n"
msgstr ""
"B<cacos>(),\n"
"B<cacosf>(),\n"
"B<cacosl>()\n"

#. type: tbl table
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: SH
#, no-wrap
msgid "EXAMPLE"
msgstr "PRZYKŁAD"

#. type: Plain text
#, no-wrap
msgid "/* Link with \"-lm\" */\n"
msgstr "/* Proszę linkować z -lm */\n"

#. type: Plain text
#, no-wrap
msgid ""
"#include E<lt>complex.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
msgstr ""
"#include E<lt>complex.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"

#. type: Plain text
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    double complex z, c, f;\n"
"    double complex i = I;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    double complex z, c, f;\n"
"    double complex i = I;\n"

#. type: Plain text
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>realE<gt> E<lt>imagE<gt>\\en\","
" argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Użycie: %s E<lt>realE<gt> E<lt>imagE<gt>\\en\","
" argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#, no-wrap
msgid "    z = atof(argv[1]) + atof(argv[2]) * I;\n"
msgstr "    z = atof(argv[1]) + atof(argv[2]) * I;\n"

#. type: Plain text
#, no-wrap
msgid "    c = cacos(z);\n"
msgstr "    c = cacos(z);\n"

#. type: Plain text
#, no-wrap
msgid "    printf(\"cacos() = %6.3f %6.3f*i\\en\", creal(c), cimag(c));\n"
msgstr "    printf(\"cacos() = %6.3f %6.3f*i\\en\", creal(c), cimag(c));\n"

#. type: Plain text
#, no-wrap
msgid "    f = -i * clog(z + i * csqrt(1 - z * z));\n"
msgstr "    f = -i * clog(z + i * csqrt(1 - z * z));\n"

#. type: Plain text
#, no-wrap
msgid "    printf(\"formula = %6.3f %6.3f*i\\en\", creal(f), cimag(f));\n"
msgstr "    printf(\"formuła = %6.3f %6.3f*i\\en\", creal(f), cimag(f));\n"

#. type: Plain text
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid "B<ccos>(3), B<clog>(3), B<complex>(7)"
msgstr "B<ccos>(3), B<clog>(3), B<complex>(7)"

#. type: SH
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
msgid ""
"This page is part of release 4.07 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.07 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
