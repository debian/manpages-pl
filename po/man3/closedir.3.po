# Polish translation of closedir.3 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 1993 David Metcalfe ("manpages-1")
# Copyright © of Polish translation:
# Adam Byrtek (PTM) <abyrtek@priv.onet.pl>, 1998.
# Robert Luberda <robert@debian.org>, 2013.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2017-02-07 21:13+0100\n"
"PO-Revision-Date: 2016-09-05 21:21+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#, no-wrap
msgid "CLOSEDIR"
msgstr "CLOSEDIR"

#. type: TH
#, no-wrap
msgid "2015-08-08"
msgstr "2015-08-08"

#. type: TH
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "closedir - close a directory"
msgstr "closedir - zamknięcie strumienia katalogu"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#, no-wrap
msgid "B<#include E<lt>sys/types.hE<gt>>\n"
msgstr "B<#include E<lt>sys/types.hE<gt>>\n"

#. type: Plain text
#, no-wrap
msgid "B<#include E<lt>dirent.hE<gt>>\n"
msgstr "B<#include E<lt>dirent.hE<gt>>\n"

#. type: Plain text
#, no-wrap
msgid "B<int closedir(DIR *>I<dirp>B<);>\n"
msgstr "B<int closedir(DIR *>I<dirp>B<);>\n"

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid ""
"The B<closedir>()  function closes the directory stream associated with "
"I<dirp>.  A successful call to B<closedir>()  also closes the underlying "
"file descriptor associated with I<dirp>.  The directory stream descriptor "
"I<dirp> is not available after this call."
msgstr ""
"Funkcja B<closedir>() zamyka strumień katalogu powiązany z I<dirp>. Pomyślne "
"wywołanie B<closedir>() zamyka także deskryptor pliku skojarzony z I<dirp>. "
"Po wywołaniu deskryptor strumienia katalogu I<dirp> staje się niedostępny."

#. type: SH
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
msgid ""
"The B<closedir>()  function returns 0 on success.  On error, -1 is returned, "
"and I<errno> is set appropriately."
msgstr ""
"Funkcja B<closedir>() w przypadku powodzenia zwraca zero. W razie "
"wystąpienia błędu zwracane jest -1 i ustawiana jest odpowiednia wartość "
"zmiennej I<errno>."

#. type: SH
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
msgid "Invalid directory stream descriptor I<dirp>."
msgstr "Nieprawidłowy deskryptor strumienia katalogu I<dirp>."

#. type: SH
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#, no-wrap
msgid "B<closedir>()\n"
msgstr "B<closedir>()\n"

#. type: tbl table
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid ""
"B<close>(2), B<opendir>(3), B<readdir>(3), B<rewinddir>(3), B<scandir>(3), "
"B<seekdir>(3), B<telldir>(3)"
msgstr ""
"B<close>(2), B<opendir>(3), B<readdir>(3), B<rewinddir>(3), B<scandir>(3), "
"B<seekdir>(3), B<telldir>(3)"

#. type: SH
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
msgid ""
"This page is part of release 4.07 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.07 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
