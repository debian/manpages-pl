# Polish translation of memcpy.3 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 1993 David Metcalfe ("manpages-1")
# Copyright © of Polish translation:
# Jarosław Beczek (PTM) <bexx@poczta.onet.pl>, 1998.
# Andrzej Krzysztofowicz (PTM) <ankry@mif.pg.gda.pl>, 2001.
# Robert Luberda <robert@debian.org>, 2014.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2017-02-07 21:15+0100\n"
"PO-Revision-Date: 2016-09-23 19:48+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#, no-wrap
msgid "MEMCPY"
msgstr "MEMCPY"

#. type: TH
#, no-wrap
msgid "2015-07-23"
msgstr "2015-07-23"

#. type: TH
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "memcpy - copy memory area"
msgstr "memcpy - kopiuje obszar pamięci"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#, no-wrap
msgid "B<void *memcpy(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"
msgstr "B<void *memcpy(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid ""
"The B<memcpy>()  function copies I<n> bytes from memory area I<src> to "
"memory area I<dest>.  The memory areas must not overlap.  Use B<memmove>(3)  "
"if the memory areas do overlap."
msgstr ""
"Funkcja B<memcpy>() kopiuje I<n> bajtów z obszaru pamięci I<src> do obszaru "
"pamięci I<dest>.  Obszary te nie mogą na siebie nachodzić. Jeżeli obszary "
"pamięci na siebie nachodzą, należy używać B<memmove>(3)."

#. type: SH
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
msgid "The B<memcpy>()  function returns a pointer to I<dest>."
msgstr "Funkcja B<memcpy>() zwraca wskaźnik do I<dest>."

#. type: SH
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#, no-wrap
msgid "B<memcpy>()\n"
msgstr "B<memcpy>()\n"

#. type: tbl table
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: SH
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#.  glibc commit 6fb8cbcb58a29fff73eb2101b34caa19a7f88eba
#.  From forward copying to backward copying
#. type: Plain text
msgid ""
"Failure to observe the requirement that the memory areas do not overlap has "
"been the source of real bugs.  (POSIX and the C standards are explicit that "
"employing B<memcpy>()  with overlapping areas produces undefined behavior.)  "
"Most notably, in glibc 2.13 a performance optimization of B<memcpy>()  on "
"some platforms (including x86-64) included changing the order in which bytes "
"were copied from I<src> to I<dest>."
msgstr ""
"Zignorowanie wymogu nienachodzenia na siebie obszarów pamięci jest źródłem "
"prawdziwych błędów (Standardy POSIX i C jednoznacznie określają, że "
"wykorzystanie B<memcpy>() z nachodzącymi na siebie obszarami pamięciami daje "
"niezdefiniowane zachowanie). Co więcej, w glibc 2.13 na niektórych "
"platformach (w tym x86-64) przeprowadzana jest optymalizacja wydajności "
"B<memcpy>() obejmująca zmianę kolejności kopiowania bajtów z I<src> do "
"I<dest>."

#.  Adobe Flash player was the highest profile example:
#.    https://bugzilla.redhat.com/show_bug.cgi?id=638477
#.    Reported: 2010-09-29 02:35 EDT by JCHuynh
#.    Bug 638477 - Strange sound on mp3 flash website
#.    https://sourceware.org/bugzilla/show_bug.cgi?id=12518
#.    Bug 12518 - memcpy acts randomly (and differently) with overlapping areas
#.    Reported:       2011-02-25 02:26 UTC by Linus Torvalds
#.  glibc commit 0354e355014b7bfda32622e0255399d859862fcd
#. type: Plain text
msgid ""
"This change revealed breakages in a number of applications that performed "
"copying with overlapping areas.  Under the previous implementation, the "
"order in which the bytes were copied had fortuitously hidden the bug, which "
"was revealed when the copying order was reversed.  In glibc 2.14, a "
"versioned symbol was added so that old binaries (i.e., those linked against "
"glibc versions earlier than 2.14)  employed a B<memcpy>()  implementation "
"that safely handles the overlapping buffers case (by providing an \"older\" "
"B<memcpy>()  implementation that was aliased to B<memmove>(3))."
msgstr ""
"Zmiana ta ujawniła poważne błędy w wielu aplikacjach przeprowadzających "
"kopiowania na nachodzących obszarach pamięci. W poprzedniej implementacji, w "
"której kolejność kopiowanych bajtów szczęśliwie ukrywała ten błąd, który "
"odsłonił się po jej odwróceniu. W glibc 2.14 dodano wersjonowany symbol, "
"dzięki czemu stare pliki binarne (zlinkowane z glibc w wersji wcześniejszej "
"niż 2.14) korzystają z implementacji B<memcpy>() która w sposób bezpieczny "
"obsługuje przypadek nachodzących obszarów pamięci (udostępniając \"starszą\" "
"implementację B<memcpy>(), która była aliasem B<memmove>(3))."

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid ""
"B<bcopy>(3), B<memccpy>(3), B<memmove>(3), B<mempcpy>(3), B<strcpy>(3), "
"B<strncpy>(3), B<wmemcpy>(3)"
msgstr ""
"B<bcopy>(3), B<memccpy>(3), B<memmove>(3), B<mempcpy>(3), B<strcpy>(3), "
"B<strncpy>(3), B<wmemcpy>(3)"

#. type: SH
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
msgid ""
"This page is part of release 4.07 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.07 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
