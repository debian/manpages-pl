# Polish translation of swaplabel.8 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 2010 Jason Borden (GPL-1)
# Copyright © of Polish translation:
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2016-08-21 17:13+0200\n"
"PO-Revision-Date: 2016-06-05 17:03+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#, no-wrap
msgid "SWAPLABEL"
msgstr "SWAPLABEL"

#. type: TH
#, no-wrap
msgid "April 2010"
msgstr "kwiecień 2010"

#. type: TH
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#, no-wrap
msgid "System Administration"
msgstr "Administracja systemem"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "swaplabel - print or change the label or UUID of a swap area"
msgstr ""
"swaplabel - wyświetla albo zmienia etykietę lub UUID przestrzeni wymiany"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
msgid "B<swaplabel> [B<-L> I<label>] [B<-U> I<UUID>] I<device>"
msgstr "B<swaplabel> [B<-L> I<etykieta>] [B<-U> I<UUID>] I<urządzenie>"

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid ""
"B<swaplabel> will display or change the label or UUID of a swap partition "
"located on I<device> (or regular file)."
msgstr ""
"B<swaplabel> wyświetla albo zmienia etykietę lub UUID partycji wymiany "
"położonej na I<urządzeniu> (lub w zwykłym pliku)."

#. type: Plain text
msgid ""
"If the optional arguments B<-L> and B<-U> are not given, B<swaplabel> will "
"simply display the current swap-area label and UUID of I<device>."
msgstr ""
"Jeśli nie podano opcjonalnych argumentów B<-L> lub B<-U>, to B<swaplabel> "
"jedynie wyświetli bieżącą etykietę i UUID przestrzeni wymiany na "
"I<urządzeniu>."

#. type: Plain text
msgid ""
"If an optional argument is present, then B<swaplabel> will change the "
"appropriate value on I<device>.  These values can also be set during swap "
"creation using B<mkswap>(8).  The B<swaplabel> utility allows to change the "
"label or UUID on an actively used swap device."
msgstr ""
"Jeśli podano opcjonalny argument, to B<swaplabel> zmieni odpowiednią wartość "
"na I<urządzeniu>. Wartości te mogą być również ustawione przy tworzeniu "
"przestrzeni wymiany za pomocą B<mkswap>(8). Narzędzie B<swaplabel> pozwala "
"zmienić etykietę lub UUID aktywnie używanego urządzenia wymiany."

#. type: SH
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
msgid "Display help text and exit."
msgstr "Wyświetla tekst pomocy i kończy pracę."

#. type: TP
#, no-wrap
msgid "B<-L>,B< --label >I<label>"
msgstr "B<-L>,B< --label >I<etykieta>"

#. type: Plain text
msgid ""
"Specify a new I<label> for the device.  Swap partition labels can be at most "
"16 characters long.  If I<label> is longer than 16 characters, B<swaplabel> "
"will truncate it and print a warning message."
msgstr ""
"Określa nową I<etykietę> urządzenia. Etykiety partycji wymiany mogą mieć "
"maksymalnie 16 znaków. Jeśli I<etykieta> będzie dłuższa, to B<swaplabel> "
"przytnie ją i wyświetli ostrzeżenie."

#. type: TP
#, no-wrap
msgid "B<-U>,B< --uuid >I<UUID>"
msgstr "B<-U>,B< --uuid >I<UUID>"

#. type: Plain text
msgid ""
"Specify a new I<UUID> for the device.  The I< UUID> must be in the standard "
"8-4-4-4-12 character format, such as is output by B<uuidgen>(1)B<.>"
msgstr ""
"Określa nowy I<UUID> urządzenia. I<UUID> musi być podany w standardowym "
"formacie 8-4-4-4-12 znaków, takim jak wyświetlany przez B<uuidgen>(1)."

#. type: SH
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
msgid ""
"B<swaplabel> was written by Jason Borden E<lt>jborden@bluehost.comE<gt> and "
"Karel Zak E<lt>kzak@redhat.comE<gt>."
msgstr ""
"B<swaplabel> został napisany przez Jasona Bordena E<lt>jborden@bluehost."
"comE<gt> i Karela Zaka E<lt>kzak@redhat.comE<gt>."

#. type: SH
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ŚRODOWISKO"

#. type: IP
#, no-wrap
msgid "LIBBLKID_DEBUG=all"
msgstr "LIBBLKID_DEBUG=all"

#. type: Plain text
msgid "enables libblkid debug output."
msgstr "włącza wyjście debugowania libblkid."

#. type: SH
#, no-wrap
msgid "AVAILABILITY"
msgstr "DOSTĘPNOŚĆ"

#. type: Plain text
msgid ""
"The swaplabel command is part of the util-linux package and is available "
"from ftp://ftp.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"Polecenie swaplabel jest częścią pakietu util-linux i jest dostępne pod "
"adresem ftp://ftp.kernel.org/pub/linux/utils/util-linux/."

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid "B<mkswap>(8), B<swapon>(8), B<uuidgen>(1)"
msgstr "B<mkswap>(8), B<swapon>(8), B<uuidgen>(1)"
