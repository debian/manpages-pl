# Polish translation of arch.1 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © 1984-2008 Free Software Foundation, Inc. (GPL-3+)
# Copyright © of Polish translation:
# Przemek Borys (PTM) <pborys@dione.ids.pl>, 1998.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2016-06-05 15:07+0200\n"
"PO-Revision-Date: 2016-04-24 22:45+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#, no-wrap
msgid "ARCH"
msgstr "ARCH"

#. type: TH
#, no-wrap
msgid "January 2016"
msgstr "styczeń 2016"

#. type: TH
#, no-wrap
msgid "GNU coreutils 8.25"
msgstr "GNU coreutils 8.25"

#. type: TH
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
msgid "arch - print machine hardware name (same as uname -m)"
msgstr "arch - wypisuje architekturę komputera (jak uname -m)"

#. type: SH
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
msgid "B<arch> [I<\\,OPTION\\/>]..."
msgstr "B<arch> [I<OPCJA>]..."

#. type: SH
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
msgid "Print machine architecture."
msgstr "Wypisuje architekturę komputera."

#. type: TP
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: SH
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
msgid "Written by David MacKenzie and Karel Zak."
msgstr "Napisane przez Davida MacKenzie i Karela Zaka."

#. type: SH
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
msgid ""
"GNU coreutils online help: E<lt>http://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>http://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
msgid ""
"Report arch translation bugs to E<lt>http://translationproject.org/team/E<gt>"
msgstr ""
"Zgłoszenia błędów w tłumaczeniu arch proszę wysyłać na adres E<lt>http://"
"translationproject.org/team/pl.htmlE<gt>"

#. type: SH
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
msgid ""
"Copyright \\(co 2016 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2016 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid "uname(1), uname(2)"
msgstr "B<uname>(1), B<uname>(2)"

#. type: Plain text
msgid ""
"Full documentation at: E<lt>http://www.gnu.org/software/coreutils/archE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>http://www.gnu.org/software/coreutils/"
"archE<gt>"

#. type: Plain text
msgid "or available locally via: info \\(aq(coreutils) arch invocation\\(aq"
msgstr "lub lokalnie, za pomocą B<info \\(aq(coreutils) arch invocation\\(aq>"

#~ msgid "September 2014"
#~ msgstr "wrzesień 2014"

#~ msgid ""
#~ "The full documentation for B<arch> is maintained as a Texinfo manual.  If "
#~ "the B<info> and B<arch> programs are properly installed at your site, the "
#~ "command"
#~ msgstr ""
#~ "Pełna dokumentacja B<arch> jest dostępna w formacie Texinfo. Jeśli "
#~ "programy B<info> i B<arch> są poprawnie zainstalowane, to polecenie"

#~ msgid "should give you access to the complete manual."
#~ msgstr "powinno dać dostęp do pełnego podręcznika."

#~ msgid "GNU coreutils 8.21"
#~ msgstr "GNU coreutils 8.21"

#~ msgid "Report arch bugs to bug-coreutils@gnu.org"
#~ msgstr ""
#~ "Zgłoszenia błędów w arch proszę wysyłać (po angielsku) na adres bug-"
#~ "coreutils@gnu.org"

#~ msgid ""
#~ "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
#~ msgstr ""
#~ "Ogólna pomoc dotycząca oprogramowania GNU: E<lt>http://www.gnu.org/"
#~ "gethelp/E<gt>"

#~ msgid ""
#~ "Copyright \\(co 2013 Free Software Foundation, Inc.  License GPLv3+: GNU "
#~ "GPL version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
#~ msgstr ""
#~ "Copyright \\(co 2013 Free Software Foundation, Inc. Licencja GPLv3+: GNU "
#~ "GPL w wersji 3 lub późniejszej E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#~| msgid "September 2011"
#~ msgid "November 2012"
#~ msgstr "listopad 2012"

#~| msgid "GNU coreutils 8.13"
#~ msgid "GNU coreutils 8.20"
#~ msgstr "GNU coreutils 8.20"

#~| msgid ""
#~| "Copyright \\(co 2011 Free Software Foundation, Inc.  License GPLv3+: GNU "
#~| "GPL version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
#~ msgid ""
#~ "Copyright \\(co 2012 Free Software Foundation, Inc.  License GPLv3+: GNU "
#~ "GPL version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
#~ msgstr ""
#~ "Copyright \\(co 2012 Free Software Foundation, Inc. Licencja GPLv3+: GNU "
#~ "GPL w wersji 3 lub późniejszej E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
