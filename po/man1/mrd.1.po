# Polish translation of mrd.1 man page
# This file is distributed under the same license as original manpage
# Copyright of the original manpage:
# Copyright © Alain Knaff (GPL-3+)
# Copyright © of Polish translation:
# Wojtek Kotwica (PTM) <wkotwica@post.pl>, 1999.
# Robert Luberda <robert@debian.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2014-11-14 09:28+0100\n"
"PO-Revision-Date: 2014-11-05 01:04+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#, no-wrap
msgid "mrd"
msgstr "mrd"

#. type: TH
#, no-wrap
msgid "09Jan13"
msgstr "9 stycznia 2013"

#. type: TH
#, no-wrap
msgid "mtools-4.0.18"
msgstr "mtools-4.0.18"

#. type: SH
#, no-wrap
msgid "Name"
msgstr "NAZWA"

#. type: Plain text
msgid "mrd - remove an MSDOS subdirectory"
msgstr "mrd - usuwa podkatalog MS-DOS"

#. type: SH
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Ostrzeżenie"

#. type: Plain text
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Ta strona podręcznika ekranowego została automatycznie wygenerowana z "
"dokumentacji texinfo pakietu mtools i może nie być kompletna  i całkowicie "
"dokładna. Szczegóły można znaleźć na końcu strony."

#. type: SH
#, no-wrap
msgid "Description"
msgstr "OPIS"

#. type: Plain text
msgid ""
"The \\&CW<mrd> command is used to remove an MS-DOS subdirectory. Its syntax "
"is:"
msgstr ""
"Polecenie \\&CW<mrd> jest używane do usuwania podkatalogu MS-DOS. Ma "
"następującą składnię:"

#. type: Plain text
#, no-wrap
msgid "I<\\&>\\&CW<mrd> [\\&CW<-v>] I<msdosdirectory> [ I<msdosdirectories>\\&... ]\n"
msgstr "I<\\&>\\&CW<mrd> [\\&CW<-v>] I<katalogmsdos> [ I<katalogimsdos>\\&... ]\n"

#. type: Plain text
msgid ""
"\\&\\&CW<Mrd> removes a directory from an MS-DOS file system. An error "
"occurs if the directory does not exist or is not empty."
msgstr ""
"\\&\\&CW<Mrd> usuwa katalog z systemu plikowego MS-DOS. Błędem jest, gdy "
"katalog nie istnieje lub nie jest pusty."

#. type: SH
#, no-wrap
msgid "See\\ Also"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
msgid "Mtools' texinfo doc"
msgstr "Dokumentacja texinfo pakietu mtools"

#. type: SH
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "PRZEGLĄDANIE DOKUMENTACJI TEXINFO"

#. type: Plain text
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Ta strona podręcznika została utworzona automatycznie z dokumentacji texinfo "
"pakietu mtools. Proces ten jednak jest tylko przybliżony i niektóre "
"elementy, jak odnośniki czy indeksy, mogą być utracone. W rzeczywistości "
"elementy te nie mają właściwych odpowiedników w formacie stron podręcznika "
"ekranowego. Ponadto nie wszystkie informacje zostały przełożone na wersję "
"podręcznika ekranowego. Dlatego zdecydowanie zalecamy użycie oryginalnej "
"dokumentacji texinfo. Na końcu niniejszej strony znajdują się instrukcje, "
"jak przeglądać dokumentację w tym formacie."

#. type: TP
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Zdatną do wydrukowania postać dokumentacji texinfo można otrzymać, "
"uruchamiając poniższe polecenia:"

#. type: Plain text
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
msgid "To generate a html copy, run:"
msgstr "Aby utworzyć wersję html, należy uruchomić:"

#. type: Plain text
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"Już utworzone wersje html można znaleźć na stronie \\&\\&CW<\\(ifhttp://www."
"gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Aby utworzyć kopię info (możliwą do przeglądania w trybie info Emacsa), "
"należy uruchomić:"

#. type: Plain text
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Dokumentacja texinfo wygląda najlepiej wydrukowana lub w postaci html. W "
"wersji info niektóre przykłady są naprawdę trudne w czytaniu z powodu "
"konwencji cytowania używanych w formacie info."
