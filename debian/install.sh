#!/bin/bash
# vim:et:ts=2:sw=2
# Based on script from Debian manpages package

set -e
set -o nounset
shopt -s nullglob

if [ "$#" != 1 ]; then
  echo "Usage: $0 /path/to/install/dir" 1>&2
  exit 1
fi

MANROOT=$1
SYMLINKS=""

install_symlink()
{
  local lsect="$1"
  local lpage="$2"
  local ltarget="$3"
  local tgtmustexist="$4"
  SYMLINKS="$1/$2 $SYMLINKS"
  if [ "${ltarget%/*}" = "$lsect" ]; then
    ltarget=${ltarget#*/}
  elif [ "${ltarget%/*}" != "${ltarget#*/}" ]; then
    ltarget=../$ltarget
  fi
  if [ "$tgtmustexist" = "Y" ] && [ ! -e "$MANROOT/$lsect/$ltarget" ] ; then
    echo -n "(skipped)"
  else
    echo -n "(symlinked)"
    ln -sf "$ltarget" "$MANROOT/$lsect/$lpage"
  fi
}


for dir in man[1-8n] generated/man[1-8]; do
  echo "-- Processing directory $dir"
  sect=${dir#generated/}

  [ -d $MANROOT/$sect ] || mkdir -p -m 755 $MANROOT/$sect

  for page in $dir/*.[1-8n]*; do
    page=${page##*/}
    echo -n "$page"
    Y=`head -n 1 "$dir/$page"`
    case "$Y" in
    .so*)
      Y="${Y#.so }"
      install_symlink "$sect" "$page" "$Y" "N"
      ;;
    *)
      install -p -m 644 "$dir/$page" "$MANROOT/$sect"
      ;;
    esac
    echo -n " "
  done
        echo
done
echo "-- Adding upstream symlinks:"
for file in *.links; do
  while read target page; do
  target="${target%.gz}"
  page="${page%.gz}"
  sect="${page%/*}"
  page="${page##*/}"
  echo -n "$page"
  install_symlink $sect $page $target "Y"
  echo -n " "
   done < $file
done
echo

echo "-- Moving and removing manpages:"
while read manpg old_s new_s; do
  # skip comments and empty lines
  if [ \( "X${manpg}" != "X" \) -a \( "X${manpg:0:1}" != "X#" \) ] ; then

    old_f="$MANROOT/man${old_s:0:1}/$manpg.$old_s"
    new_f="$MANROOT/man${new_s:0:1}/$manpg.$new_s"

    echo -n "$manpg.$old_s"
    if [ -e "$old_f" ] ; then
      if [ "X$new_s" = "X" ] ; then
        echo -n "(removed)"
      else
        if [ "x$old_f" = "x$new_f" ]; then
          mv "$old_f" "$old_f.tmp"
          old_f="$old_f.tmp"
        fi

        perl -pe '$i+= s;^(\.TH\s+\S+\s+)\S+(\s+.*);${1}'"${new_s}"'${2};gi unless $i>0' \
          < "$old_f" > "$new_f"

        touch -r "$old_f" "$new_f"

        if [ "x$old_s" = "x$new_s" ]; then
          cmp -s "$old_f" "$new_f" \
           && echo -n "(entry ignored)" || echo -n "(corrected .TH line)"
        else
          echo -n "(moved to section $new_s)"
        fi
      fi

      rm -f "$old_f"

    else
      echo -n "(not found!)"
    fi
    echo -n " "
  fi
done < debian/move.list
echo

echo "-- Removing dangling symlinks:"
for i in $SYMLINKS; do
  i="$MANROOT/$i"
  if [ -L "$i" -a ! -e "$i" ]; then
    echo -n "${i##*/}(removed) "
    rm -f "$i"
  fi
done
echo
rm -f debian/tmp.stamp

echo
