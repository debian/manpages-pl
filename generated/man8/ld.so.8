.\" %%%LICENSE_START(PUBLIC_DOMAIN)
.\" This is in the public domain
.\" %%%LICENSE_END
.\"
.\"*******************************************************************
.\"
.\" This file was generated with po4a. Translate the source file.
.\"
.\"*******************************************************************
.\" This file is distributed under the same license as original manpage
.\" Copyright of the original manpage:
.\" Copyright © 
.\" Copyright © of Polish translation:
.\" Przemek Borys (PTM) <pborys@dione.ids.pl>, 1998.
.\" Grzegorz Goławski (PTM) <grzegol@pld.org.pl>, 2002.
.\" Robert Luberda <robert@debian.org>, 2014.
.\" Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
.TH LD.SO 8 2015\-12\-28 GNU "Podręcznik programisty Linuksa"
.SH NAZWA
ld.so, ld\-linux.so* \- dynamiczny konsolidator/ładowacz
.SH SKŁADNIA
Konsolidator dynamiczny może być uruchomiony albo pośrednio przez
uruchomienie jakiegokolwiek skonsolidowanego dynamicznie programu lub
obiektu dzielonego (w tym przypadku żadna opcja linii poleceń nie będzie
przekazana do konsolidatora dynamicznego i w przypadku ELF\-a uruchomiony
zostanie konsolidator dynamiczny przechowywany w sekcji \fB.interp\fP
programu), albo bezpośrednio przez uruchomienie:
.P
\fI/lib/ld\-linux.so.*\fP [OPCJE] [PROGRAM [ARGUMENTY]]
.SH OPIS
Programy \fBld.so\fP i \fBld\-linux.so*\fP wyszukują i uruchamiają obiekty dzielone
(biblioteki dzielone) wymagane przez program, przygotowują program do
uruchomienia, a w końcu go uruchamiają.
.LP
Binarki linuksowe wymagają konsolidacji dynamicznej (czyli konsolidacji
podczas uruchamiania), chyba że podczas kompilacji programowi \fBld\fP(1)
przekazano opcję\ \fB\-static\fP.
.LP
Program \fBld.so\fP obsługuje binarki w używanym dawno temu formacie a.out;
\fBld\-linux.so*\fP obsługuje format ELF (\fI/lib/ld\-linux.so.1\fP dla libc5,
\fI/lib/ld\-linux.so.2\fP dla glibc2), który jest używany przez wszystkich już
od ładnych paru lat. Poza tym oba programy zachowują się tak samo i używają
tych samych plików pomocniczych i programów: \fBldd\fP(1), \fBldconfig\fP(8) oraz
\fI/etc/ld.so.conf\fP.
.LP
Podczas rozwiązywania zależności obiektów dzielonych, konsolidator
dynamiczny najpierw przegląda każdy łańcuch znaków zależności w poszukiwaniu
znaku ukośnika (może wystąpić, jeśli podczas linkowania obiektu dzielonego
podano nazwę ścieżki zawierającą ukośniki). Jeśli taki znak zostanie
znaleziony, to łańcuch znaków zależności jest interpretowany jako nazwa
(względnej lub bezwzględnej) ścieżki i obiekt dzielony jest ładowany,
używając tej nazwy ścieżki.
.LP
Jeśli zależność od obiektu dzielonego nie zawiera znaku ukośnika, to
potrzebne biblioteki dzielone są szukane w następującej kolejności:
.IP o 3
(Tylko ELF). Używa katalogów podanych w sekcji atrybutów dynamicznych
DT_RPATH binarki, jeśli jest obecna i nie istnieje atrybut
DT_RUNPATH. Użycie DT_RPATH jest przestarzałe.
.IP o
Przy użyciu zmiennej środowiskowej \fBLD_LIBRARY_PATH\fP, chyba że plik
wykonywalny jest uruchomiony w trybie bezpiecznego wykonania; (zob. niżej),
gdy jest ignorowana.
.IP o
(Tylko ELF) Używając katalogów podanych w sekcji atrybutów dynamicznych
DT_RUNPATH binarki, jeśli taka sekcja istnieje.
.IP o
Z pliku bufora \fI/etc/ld.so.cache\fP, zawierającego skompilowaną
listę\ obiektów dzielonych poprzednio znalezionych w ścieżce obiektów
dzielonych. Jeśli jednakże program binarny został skonsolidowany z opcją
linkera  \fB\-z nodeflib\fP, to pomijane są obiekty dzielone z domyślnych
ścieżek obiektów dzielonych. Preferowane są obiekty dzielone zainstalowane w
katalogach zgodnych z właściwościami sprzętu (patrz niżej).
.IP o
W domyślnej ścieżce \fI/lib\fP a potem w \fI/usr/lib\fP (na niektórych
architekturach 64\-bitowych domyślną ścieżką 64\-bitowych obiektów dzielonych
jest \fI/lib64\fP, a potem \fI/usr/lib64\fP). Ten krok jest pomijany, jeśli
program binarny został skonsolidowany z opcją\ \fB\-z nodeflib\fP konsolidatora.
.SS "Rozwijanie zmiennych w rpath"
.PP
\fBld.so\fP rozumie pewne łańcuchy znaków w specyfikacji rpath (DT_RPATH lub
DT_RUNPATH); łańcuch ty są zamieniane następująco:
.TP 
\fI$ORIGIN\fP (lub równoważnie \fI${ORIGIN}\fP)
Rozwija się do katalogu zawierającego program lub obiekt dzielony. Tak więc
aplikacja znajdująca się w \fIjakimśkatalogu/app\fP może zostać skompilowana z

    gcc \-Wl,\-rpath,\(aq$ORIGIN/../lib\(aq

tak żeby przypisane jej obiekty dzielone mogły być\ umieszczone w
\fIjakimśkatalogu/lib\fP, niezależnie od tego, gdzie \fIjakiśkatalog\fP jest
umieszczony w hierarchii katalogów. Umożliwia to tworzenie aplikacji, które
nie muszą być instalowane w specjalnych katalogów, ale mogą po prostu być
rozpakowane w dowolnym katalogu i wciąż\ będą mogły znaleźć swoje obiekty
dzielone.
.TP 
\fI$LIB\fP (lub równoważnie \fI${LIB}\fP)
Rozwija się do \fIlib\fP lub \fIlib64\fP w zależności od architektury (np. na
x86\-64 rozwija się \fIlib64\fP, a na x86\-32 rozwija się do \fIlib\fP).
.TP 
\fI$PLATFORM\fP (lub równoważnie \fI${PLATFORM}\fP)
.\" To get an idea of the places that $PLATFORM would match,
.\" look at the output of the following:
.\"
.\"     mkdir /tmp/d
.\"     LD_LIBRARY_PATH=/tmp/d strace -e open /bin/date 2>&1 | grep /tmp/d
.\"
.\" ld.so lets names be abbreviated, so $O will work for $ORIGIN;
.\" Don't do this!!
Rozwija się do łańcucha znaków odpowiadającemu typowi procesora systemu
(np. "x86_64"). Na niektórych architekturach jądro Linuksa nie przekazuje
konsolidatorowi dynamicznemu oznaczenia platformy. Wartość tego łańcucha
znaków jest pobierana z wartości \fBAT_PLATFORM\fP pomocniczego wektora (patrz
\fBgetauxval\fP(3)).
.SH OPCJE
.TP 
\fB\-\-list\fP
Wyświetla wszystkie zależności wraz ze sposobem ich rozwiązania.
.TP 
\fB\-\-verify\fP
Sprawdza, że program jest konsolidowany dynamicznie i że ten konsolidator
dynamiczny może go obsłużyć.
.TP 
\fB\-\-inhibit\-cache\fP
Nie używa \fI/etc/ld.so.cache\fP.
.TP 
\fB\-\-library\-path\fP \fIścieżka\fP
Używa \fIścieżki\fP zamiast ustawienia zmiennej środowiska \fBLD_LIBRARY_PATH\fP
(patrz niżej).
.TP 
\fB\-\-inhibit\-rpath\fP \fIlista\fP
Ignoruje informacje RPATH i RUNPATH w nazwach obiektów w \fIliście\fP. Opcja
jest ignorowana podczas działania w trybie bezpiecznego wykonania
(zob. niżej)
.TP 
\fB\-\-audit\fP \fIlista\fP
Używa obiektów wymienionych w \fIliście\fP jako audytorów.
.SH ŚRODOWISKO
.\"
Różne zmienne środowiska wpływają na działanie dynamicznego konsolidatora
.SS "Tryb bezpiecznego wykonania"
Ze względów bezpieczeństwa, działania niektórych zmiennych środowiskowych
jest anulowane lub modyfikowane, jeśli dynamiczny konsolidator określi, że
plik binarny powinien być\ uruchomiony w trybie bezpiecznego wykonania. To
określenie dokonywane jest na podstawie sprawdzenia, czy wpis \fBAT_SECURE\fP w
dodatkowym wektorze (zob. \fBgetauxval\fP(3)) ma niezerową wartość. Wpis ten
może mieć niezerową wartość z różnych powodów, m.in.:
.IP * 3
Rzeczywisty i efektywny identyfikator użytkownika procesu różnią się lub
rzeczywisty i efektywny identyfikator grupy różnią\ się. Zwykle ma to miejsce
jako rezultat wykonywania programu z set\-user\-ID lub set\-group\-ID.
.IP *
Proces użytkownika z identyfikatorem niebędącym ID roota wykonuje plik
binarny z nadanymi przywilejami permitted lub effective.
.IP *
.\"
Niezerowa wartość mogła być\ ustawiona przez linuksowy  moduł bezpieczeństwa
\- Linux Security Module.
.SS "Zmienne środowiskowe"
Wśród ważniejszych zmiennych środowiskowych są następujące:
.TP 
\fBLD_ASSUME_KERNEL\fP (glibc od wersji 2.2.3)
Każdy obiekt dzielony może informować konsolidator dynamiczny o wymaganej
minimalnej wersji ABI jądra (To wymaganie jest zakodowane w sekcji "note"
ELF\-a; sekcja ta jest widoczna w \fIreadelf\ \-n\fP jako sekcja oznaczona
\fBNT_GNU_ABI_TAG\fP). Podczas działania konsolidator dynamiczny określa wersję
ABI uruchomionego jądra i odrzuca obiekty dzielone, które wymagają
minimalnej wersji ABI większej niż wersja ABI uruchomionego jądra.

\fBLD_ASSUME_KERNEL\fP może zostać użyta do spowodowania, że konsolidator
dynamiczny założy, że jest uruchomiony na systemie z inną wersją\ ABI
jądra. Na przykład następująca linia poleceń powoduje, że konsolidator
dynamiczny podczas ładowania obiektów dzielonych wymaganych przez \fImójprog\fP
zakłada, że działa w systemie Linux 2.2.5

.in +4n
.nf
$ \fBLD_ASSUME_KERNEL=2.2.5 ./mójprog\fP
.fi
.in

W systemach, które dostarczają wielu wersji obiektu dzielonego (w różnych
katalogach w ścieżce wyszukiwania) o różnych minimalnych wymaganiach wersji
ABI jądra, \fBLD_ASSUME_KERNEL\fP może zostać użyte do wybrania tej wersji
obiektu, która zostanie użyta (w zależności od porządku przeszukiwania
katalogów). Historycznie najczęstszym użyciem \fBLD_ASSUME_KERNEL\fP było
ręczne wybieranie starszej implementacji POSIX\-owych wątków LinuxThreads w
systemach, które miały zainstalowane zarówno LinuxThreads, jak i NPTL (który
był domyślną wersją na takich systemach); patrz \fBpthreads\fP(7).
.TP 
\fBLD_BIND_NOW\fP
(libc5; glibc od wersji 2.1.1) Gdy zmienna ta jest obecna, sprawia, że
dynamiczny konsolidator rozwiąże wszystkie symbole podczas startu programu,
a nie wtedy gdy będzie do nich pierwsze odniesienie. Jest to użyteczne
podczas używania debuggera.
.TP 
\fBLD_LIBRARY_PATH\fP
Lista katalogów, w których szukać bibliotek ELF podczas
wykonywania. Składniki listy mogą\ być\ oddzielone dwukropkiem lub
średnikiem. Podobne do zmiennej środowiskowej \fBPATH\fP. Ta zmienna jest
ignorowana w trybie bezpiecznego wykonania.
.TP 
\fBLD_PRELOAD\fP
Lista dodatkowych, podanych przez użytkownika obiektów dzielonych ELF, którą
należy załadować przed wszystkimi innymi. Elementy listy mogą być oddzielone
od siebie spacjami lub dwukropkami. Umożliwia to wybiórczą zamianę funkcji w
innych obiektach dzielonych. Może być używane do wybiórczego nadpisywania
funkcji z innych obiektów dzielonych. Obiekty są wyszukiwane zgodnie z
regułami podanymi w rozdziale OPIS. W trybie bezpiecznego wykonania,
załadowane wstępnie ścieżki zawierające ukośniki są ignorowane, a obiekty
dzielone ze standardowej ścieżki katalogów ładowane będą tylko wtedy, gdy
mają także ustawiony bit set\-user\-ID.
.TP 
\fBLD_TRACE_LOADED_OBJECTS\fP
(tylko ELF) Gdy zmienna ta jest ustawiona (na dowolną wartość) powoduje, że
program wypisze swoje dynamiczne zależności, tak jakby był uruchomiany przez
\fBldd\fP(1), a nie normalnie.
.LP
Jest także wiele bardziej lub mniej mętnych zmiennych, wiele przestarzałych
lub przeznaczonych do użytku wewnętrznego.
.TP 
\fBLD_AOUT_LIBRARY_PATH\fP
(libc5) Wersja \fBLD_LIBRARY_PATH\fP tylko dla binariów a.out. Starsze wersje
ld\-linux.so.1 wspierały także \fBLD_ELF_LIBRARY_PATH\fP.
.TP 
\fBLD_AOUT_PRELOAD\fP
(libc5) Wersja \fBLD_PRELOAD\fP tylko dla binariów a.out. Starsze wersje
ld\-linux.so.1 wspierały także \fBLD_ELF_PRELOAD\fP.
.TP 
\fBLD_AUDIT\fP
(glibc od wersji 2.4). Rozdzielona dwukropkami lista określonych przez
użytkownika dzielonych obiektów ELF do załadowania przez wszystkimi innymi
obiektami w oddzielnej przestrzeni nazw konsolidatora (to jest w przestrzeni
nazw, która nie wpływa na przyporządkowania symboli, które odbywa się w
procesie). Obiektów tych można użyć\ do audytu operacji konsolidatora
dynamicznego. \fBLD_AUDIT\fP jest ignorowane w trybie bezpiecznego wykonania.

Konsolidator dynamiczny powiadomi obiekty dzielone audytu w tak zwanych
punktach sprawdzeń audytu \em na przykład ładowanie nowego dzielonego
obiektu, rozwiązanie symbolu lub wywołanie symbolu z innego obiektu
dzielonego \em przez wywołanie odpowiedniej funkcji obiektu
dzielonego. Szczegóły można znaleźć w \fBrtld\-audit\fP(7). Interfejs audytu
jest w dużej mierze zgodny z tym udostępnianym przez Solarisa, opisanym w
jego \fIPrzewodniku po konsolidatorze i bibliotekach\fP w rozdziale \fIInterfejs
audytu konsolidatora\fP.
.TP 
\fBLD_BIND_NOT\fP
(glibc od wersji 2.1.95) Jeśli ta zmienna środowiskowa jest ustawiona na
niepusty łańcuch, to nie zachodzi aktualizacja GOT (global offset table) ani
PLT (procedure linkage table) po rozwinięciu symbolu.
.TP 
\fBLD_DEBUG\fP
(glibc od wersji 2.1) Wypisuje rozwlekłe informacje debugowania
konsolidatora dynamicznego. Jeśli ustawione na \fBall\fP wypisuje wszystkie
dostępne informacje debugowania, jeśli ustawione na \fBhelp\fP wyświetla listę
kategorii, które można podać jako wartość tej zmiennej środowiskowej. Od
glibc wersji 2.3.4 \fBLD_DEBUG\fP jest ignorowana w trybie bezpiecznego
wykonania, chyba że istnieje plik \fI/etc/suid\-debug\fP (jest zawartość jest
nieistotna).
.TP 
\fBLD_DEBUG_OUTPUT\fP
(glibc od wersji 2.1) Plik, do którego będzie zapisane wyjście
\fBLD_DEBUG\fP. Domyślnie jest to standardowe wyjście
błędów. \fBLD_DEBUG_OUTPUT\fP jest ignorowane w trybie bezpiecznego wykonania.
.TP 
\fBLD_DYNAMIC_WEAK\fP
.\" See weak handling
.\"     https://www.sourceware.org/ml/libc-hacker/2000-06/msg00029.html
.\"     To: GNU libc hacker <libc-hacker at sourceware dot cygnus dot com>
.\"     Subject: weak handling
.\"     From: Ulrich Drepper <drepper at redhat dot com>
.\"     Date: 07 Jun 2000 20:08:12 -0700
.\"     Reply-To: drepper at cygnus dot com (Ulrich Drepper)
(glibc od wersji 2.1.91) Jeśli ta zmienna środowiskowa jest zdefiniowana (z
dowolną\ wartością), to pozwala na nadpisywanie słabych (ang. weak) symboli
(przywracając poprzednie zachowanie glibc). Od wersji 2.3.4 biblioteki glibc
\fBLD_DYNAMIC_WEAK\fP jest ignorowana w trybie bezpiecznego wykonania.
.TP 
\fBLD_HWCAP_MASK\fP
(glibc od wersji 2.1) Maska właściwości sprzętowych.
.TP 
\fBLD_KEEPDIR\fP
(tylko a.out)(libc5)  Nie ignoruju katalogu w nazwach ładowanych bibliotek
a.out. Używanie tej opcji nie jest zalecane.
.TP 
\fBLD_NOWARN\fP
(tylko a.out)(libc5) Powstrzymuje ostrzeżenia o bibliotekach a.out o
niekompatybilnych numerach minorowych wersji.
.TP 
\fBLD_ORIGIN_PATH\fP
.\" Only used if $ORIGIN can't be determined by normal means
.\" (from the origin path saved at load time, or from /proc/self/exe)?
(glibc od wersji 2.1) Ścieżka, w której znaleziono program binarny (dla
programów bez bitu set\-user\-ID). Od wersji 2.4 biblioteki glibc
\fBLD_ORIGIN_PATH\fP jest ignorowana w trybie bezpiecznego wykonania.
.TP 
\fBLD_POINTER_GUARD\fP
.\" commit a014cecd82b71b70a6a843e250e06b541ad524f7
(glibc od wersji 2.4 do 2.22) Ustawione na 0 powoduje wyłączenie ochrony
wskaźników. Jakakolwiek inna wartość włącza ochronę wskaźników, co jest
także zachowaniem domyślnym. Ochrona wskaźników jest mechanizmem
bezpieczeństwa, w którym niektóre wskaźniki do kodu przechowywanego w
zapisywalnej pamięci programu (adresy powrotu zwrócone przez \fBsetjmp\fP(3)
lub wskaźniki do funkcji używane przez różne funkcje wewnętrzne biblioteki
glibc) są w sposób pseudolosowy zmieniane, aby utrudnić hakerowi przejęcie
tych wskaźników i przeprowadzenie ataków typu przepełnienie bufora lub
stosu. Od glibc 2.23 \fBLD_POINTER_GUARD\fP nie można już użyć do wyłączenia
ochrony wskaźników, która jest teraz zawsze aktywna.
.TP 
\fBLD_PROFILE\fP
(glibc od wersji 2.1) Nazwa (pojedynczego) obiektu dzielonego przeznaczonego
do profilowania, podane albo jako nazwa ścieżki, albo nazwa pliku
so. Wyjście profilowania jest dopisywane do pliku o nazwie
"\fI$LD_PROFILE_OUTPUT\fP/\fI$LD_PROFILE\fP.profile".
.TP 
\fBLD_PROFILE_OUTPUT\fP
(glibc od wersji 2.1) Katalog, w którym powinno być zapisane wyjście
\fBLD_PROFILE\fP. Jeśli ta zmienna nie jest zdefiniowana lub jeśli wartość tej
zmiennej jest pusta, to domyślnym katalogiem jest
\fI/var/tmp\fP. \fBLD_PROFILE_OUTPUT\fP jest ignorowane w trybie bezpiecznego
wykonania; wówczas zawsze używany jest \fI/var/profile\fP.
.TP 
\fBLD_SHOW_AUXV\fP
(glibc od wersji 2.1) Jeśli ta zmienna środowiskowa jest zdefiniowana (z
dowolną\ wartością), wyświetla tablicę pomocniczą przekazaną przez jądro. Od
wersji 2.3.5 biblioteki glibc \fBLD_SHOW_AUXV\fP jest ignorowana w trybie
bezpiecznego wykonania.
.TP 
\fBLD_TRACE_PRELINKING\fP
.\" (This is what seems to happen, from experimenting)
(glibc od wersji 2.4) Jeśli ta zmienna środowiskowa jest zdefiniowana (z
dowolną\ wartością) śledzi prekonsolidację obiektu, którego nazwa jest
przypisana do tej zmiennej środowiskowej (\fBldd\fP(1) służy do pozyskania
listy obiektów, które mogą\ być\ śledzone). Jeśli nazwa obiektu nie zostanie
rozpoznana, to śledzona jest cała aktywność prekonsolidacji.
.TP 
\fBLD_USE_LOAD_BIAS\fP
.\" http://sources.redhat.com/ml/libc-hacker/2003-11/msg00127.html
.\" Subject: [PATCH] Support LD_USE_LOAD_BIAS
.\" Jakub Jelinek
Domyślnie (czyli jeśli ta zmienna nie jest zdefiniowana) programy
wykonywalne i prekonsolidowane obiekty dzielone będą\ uwzględniały adresy
bazowe obiektów dzielonych, od których zależą, a (nieprekonsolidowane)
programy wykonywalne niezależne od pozycji (position\-independent executable,
PIE) i inne obiekty dzielone nie będą ich uwzględniały. Jeśli
\fBLD_USE_LOAD_BIAS\fP jest zdefiniowana z jakąś wartością, to zarówno programy
wykonywalne, jak i PIE, będą uwzględniały adresy bazowe. Jeśli
\fBLD_USE_LOAD_BIAS\fP jest zdefiniowana z wartością 1, to ani programy
wykonywalne, ani PIE nie będą uwzględniały adresów bazowych. Zmienna ta jest
ignorowana w trybie bezpiecznego wykonania.
.TP 
\fBLD_VERBOSE\fP
(glibc od wersji 2.1). Jeśli ustawione na niepusty łańcuch znaków i jeśli
została ustawiona zmienna środowiskowa \fBLD_TRACE_LOADED_OBJECTS\fP, to
wypisuje informacje o wersjonowaniu symboli programu.
.TP 
\fBLD_WARN\fP
(tylko ELF)(glibc od wersji 2.1.3) Jeśli ustawione na niepusty łańcuch
znaków, to włącza ostrzeganie o nierozwijalnych symbolach.
.TP 
\fBLD_PREFER_MAP_32BIT_EXEC\fP
(tylko x86\-64)(glibc w wersji od 2.23) Zgodnie z przewodnikiem optymalizacji
oprogramowania Intel Silvermont, dla aplikacji 64\-bitowych wydajność
przewidywania rozgałęzień może być zmniejszona, gdy cel gałęzi jest oddalony
o ponad 4GB. Jeśli ta zmienna środowiskowa jest ustawiona (na
dowolną\ wartość), to \fBld.so\fP spróbuje najpierw przypisać flagi
wykonywalności za pomocą flagi \fBMAP_32BIT\fP \fBmmap\fP(2), a jeśli się to nie
powiedzie \(em bez tej flagi. Przy okazji: MAP_32BIT przypisze niższe 2GB
(nie 4GB) przestrzeni adresowej. Ponieważ \fBMAP_32BIT\fP redukuje zakres
adresowy dostępny dla losowego rozmieszczania obszarów pamięci (ang. ASLR \-
adress space layout randomization), w trybie bezpiecznego wykonania
\fBLD_PREFER_MAP_32BIT_EXEC\fP jest zawsze wyłączona.
.TP 
\fBLDD_ARGV0\fP
(libc5) \fIargv\fP[0] do użycia przez \fBldd\fP(1), jeśli żadne argumenty nie są
obecne.
.SH PLIKI
.PD 0
.TP 
\fI/lib/ld.so\fP
Dynamiczny konsolidator/ładowacz a.out
.TP 
\fI/lib/ld\-linux.so.\fP{\fI1\fP,\fI2\fP}
Dynamiczny konsolidator/ładowacz ELF
.TP 
\fI/etc/ld.so.cache\fP
Plik zawierający skompilowaną listę katalogów, w których należy szukać
obiektów dzielonych oraz uporządkowaną listę kandydujących obiektów
dzielonych.
.TP 
\fI/etc/ld.so.preload\fP
Plik zawierający oddzieloną spacjami listę obiektów dzielonych ELF, które
mają być załadowane przed programem.
.TP 
\fBlib*.so*\fP
obiekty dzielone
.PD
.SH UWAGI
Możliwości \fBld.so\fP dostępne są tylko dla programów binarnych,
skompilowanych przy użyciu libc w wersji 4.4.3 lub wyższej. Funkcjonalności
ELF\-a są dostępne od Linuksa 1.1.52 i libc5.
.SS "Możliwości sprzętowe"
Niektóre obiekty dzielone są kompilowane z użyciem specyficznych dla danego
sprzętu instrukcji, które nie muszą istnieć na każdym CPU. Takie
obiektypowinny być\ instalowane w katalogach, których nazwy określają
wymagane właściwości sprzętu, na przykład \fI/usr/lib/sse2/\fP. Konsolidator
dynamiczny porównuje nazwy takich katalogów ze sprzętem maszyny i wybiera
najbardziej odpowiednią wersję danego obiektu dzielonego. Katalogi
właściwości sprzętu mogą tworzyć kaskady, dopuszczając kombinacje cech
CPU. Lista nazw wspieranych właściwości sprzętowych zależy od CPU. Obecnie
rozpoznawane są następujące nazwy:
.TP 
\fBAlpha\fP
ev4, ev5, ev56, ev6, ev67
.TP 
\fBMIPS\fP
loongson2e, loongson2f, octeon, octeon2
.TP 
\fBPowerPC\fP
4xxmac, altivec, arch_2_05, arch_2_06, booke, cellbe, dfp, efpdouble,
efpsingle, fpu, ic_snoop, mmu, notb, pa6t, power4, power5, power5+, power6x,
ppc32, ppc601, ppc64, smt, spe, ucache, vsx
.TP 
\fBSPARC\fP
flush, muldiv, stbar, swap, ultra3, v9, v9v, v9v2
.TP 
\fBs390\fP
dfp, eimm, esan3, etf3enh, g5, highgprs, hpage, ldisp, msa, stfle, z900,
z990, z9\-109, z10, zarch
.TP 
\fBx86 (tylko 32\-bitowe)\fP
acpi, apic, clflush, cmov, cx8, dts, fxsr, ht, i386, i486, i586, i686, mca,
mmx, mtrr, pat, pbe, pge, pn, pse36, sep, ss, sse, sse2, tm
.SH "ZOBACZ TAKŻE"
.\" .SH AUTHORS
.\" ld.so: David Engel, Eric Youngdale, Peter MacDonald, Hongjiu Lu, Linus
.\"  Torvalds, Lars Wirzenius and Mitch D'Souza
.\" ld-linux.so: Roland McGrath, Ulrich Drepper and others.
.\"
.\" In the above, (libc5) stands for David Engel's ld.so/ld-linux.so.
\fBld\fP(1), \fBldd\fP(1), \fBpldd\fP(1), \fBsprof\fP(1), \fBdlopen\fP(3), \fBgetauxval\fP(3),
\fBcapabilities\fP(7), \fBrtld\-audit\fP(7), \fBldconfig\fP(8), \fBsln\fP(8)
.SH "O STRONIE"
Angielska wersja tej strony pochodzi z wydania 4.05 projektu Linux
\fIman\-pages\fP. Opis projektu, informacje dotyczące zgłaszania błędów, oraz
najnowszą wersję oryginału można znaleźć pod adresem
\%https://www.kernel.org/doc/man\-pages/.
.SH TŁUMACZENIE
Autorami polskiego tłumaczenia niniejszej strony podręcznika man są:
Przemek Borys (PTM) <pborys@dione.ids.pl>,
Grzegorz Goławski (PTM) <grzegol@pld.org.pl>,
Robert Luberda <robert@debian.org>
i
Michał Kułach <michal.kulach@gmail.com>.
.PP
Polskie tłumaczenie jest częścią projektu manpages-pl; uwagi, pomoc, zgłaszanie błędów na stronie http://sourceforge.net/projects/manpages-pl/. Jest zgodne z wersją \fB 4.05 \fPoryginału.
